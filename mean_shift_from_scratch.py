import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
import pandas as pd
from sklearn import preprocessing

def handle_non_numeric_data(df):
       columns = df.columns.values
  
       for column in columns:
           if df[column].dtype !=np.int64 and df[column].dtype !=np.float64:
                elements = df[column].values
                text_vals = {}
                def convert_to_int(val):
                     return text_vals[val]
                unique_elements = set(elements)
                x = 0
                for i in unique_elements:
                     if i not in text_vals:
                         text_vals[i] = x 
                 
                df[column]= list(map(convert_to_int,df[column]))
       
       return df
 


class MeanShift: 
      def __init__(self,tol = 0.001,max_iter = 300):
             self.tol = tol
             self.max_iter = max_iter
             
      def fit(self,data):
             self.centroids = {}
             for i in range(len(data)):
                 self.centroids[i]=data[i]
             self.radius = 3.0
             centroid =dict(self.centroids)
             while True:
		     new_centroids = []
		     for c in centroid:
		           in_bandwidth = [] 
		           
		           for i in range(len(data)):
		               if np.linalg.norm(data[i] - centroid[c]) < self.radius :
		                    in_bandwidth.append(tuple(data[i]))
                                
		           new_centroid = np.average(in_bandwidth,axis = 0 )
                           new_centroids.append(tuple(new_centroid))
		      
		     previous_centroid = dict(centroid)
		     unique = sorted(list(set(new_centroids)))
		     centroid = {}
		     for i in range(len(unique)):
		         centroid[i] = np.array(unique[i])
	 
		     optimized = True
		    
		     for i in centroid:
		         if not np.array_equal(centroid[i],previous_centroid[i]):
		                optimized = False
		         if not optimized :
		              break
		     if optimized:
		         break
		     
	     self.centroids = centroid
             self.classifications = []
             for i in data:
                distances = [np.linalg.norm(i - self.centroids[centroid]) for centroid in self.centroids]
                classification = distances.index(min(distances))
                self.classifications.append(classification)

      def predict(self,data):
             self.classifications = {}
             distances = [np.linalg.norm(data - self.centroids[centroid]) for centroid in self.centroids]
             classification = distances.index(min(distances))
             return classification

  
df = pd.read_excel("titanic3.xls")
original_df = pd.DataFrame.copy(df)
#print original_df.head()

#prit df.head()
df.drop(['name','boat'],1,inplace = "True")
df.fillna(0,inplace = True)


df = handle_non_numeric_data(df) 

X = np.array(df.drop(['survived'],1)).astype(float)

X = preprocessing.scale(X)            

Y = np.array(df['survived'].astype(float))


clf=MeanShift()
clf.fit(X)
correct =0.0
"""
for i in range(len(clf.classifications)):
     if clf.classifications[i] == Y[i]:
            correct += 1
print correct/len(clf.classifications)

"""
#print clf.centroids

original_df['cluster_group'] = np.nan
for i in range(len(clf.classifications)):
      original_df['cluster_group'].iloc[i]= clf.classifications[i]

print (original_df[original_df['cluster_group']==4].describe())

#print original_df.head()
       
