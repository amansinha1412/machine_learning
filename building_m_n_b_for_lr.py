import numpy as np
from statistics import mean
import matplotlib.pyplot as plt
from matplotlib import style
import random
style.use('ggplot')

#xs=np.array([1,2,36,4,59,6],dtype=np.float64)
#ys=np.array([14,45,6,7,85,9],dtype=np.float64)

def best_fit_slope(x,y):
   p1=mean(x)*mean(y)
   p2=mean(x*y)
   p3=mean(x)**2
   p4=mean(x**2)
   m=(p1-p2)/(p3-p4)
   b=mean(y)-m*(mean(x))
   return m,b
 
def coeff(ys,regression_line):
    sum1=0
    sum2=0
    for k in range(len(ys)):
        sum1= sum1 + (regression_line[k]-ys[k])**2
    
    y_mean=mean(ys)
    for k in range(len(ys)):
        sum2 = sum2 + (y_mean-ys[k])**2
    
    ratio=sum1/sum2
    coeffecient_of_det= 1 - ratio
    return coeffecient_of_det  
     
def create_dataset(hm,variance,step=2,correlation=False):
    val=1
    ys=[]
    for i in range(hm):
        y= val + random.randrange(-variance,variance)
        ys.append(y)
        if correlation and correlation == 'pos':
             val = val+step
        elif correlation and correlation =='neg':
             val = val-step

    xs=[i for i in range(len(ys))]
    
    return np.array(xs,dtype=np.float64),np.array(ys,dtype=np.float64)

    
    
xs,ys=create_dataset(40,40,2,correlation='pos')
print (xs,ys)
m,b=best_fit_slope(xs,ys)

regression_line=[]
for k in xs:
  y= m*k + b
  regression_line.append(y)

r_squared=coeff(ys,regression_line)

print (r_squared)


"""
predict_x=7
predict_y=(m*predict_x)+b
plt.scatter(predict_x,predict_y,color='#003F72', label='prediction')
plt.scatter(xs,ys,label='data')
plt.plot(xs,regression_line,label='regression_line')
plt.legend(loc=4)
plt.show()

"""
#print (m)

