from collections import Counter
import warnings
import numpy as np
import pandas as pd
import random
"""
df=pd.read_csv("breast-cancer-wisconsin.data")
df.replace('?',-99999,inplace=True)
#df.dropna(inplace=True)
df=df.drop([id],1)
"""
def k_nearest_neighbors(dataset,predict,k=3):
    if (len(dataset)>=k):
          warnings.warn("the no of groups is less than or equal to k")

    distances=[]
    for groups in dataset:
        for features in dataset[groups]:
            y=np.linalg.norm(np.array(features)-np.array(predict))
            distances.append([y,groups])
        
    votes=[i[1] for i in sorted(distances)[:k]]
    #print votes
    #print Counter(votes).most_common(1)
    vote_result = Counter(votes).most_common(1)[0][0]
    return vote_result

"""
dataset={'2':[[1,2],[3,4],[5,6]],
         '4':[[7,8],[9,10],[11,12]]}
new_features=[5,7]
"""
accuracies=[]
for i in range(5):   
	df=pd.read_csv("breast-cancer-wisconsin.data")
	df.replace('?',-99999,inplace=True)
	df.drop(['id'],1,inplace=True)
	full_data=df.astype(float).values.tolist()
	#print full_data
	#print df.head()

	random.shuffle(full_data)

	test_size=0.3
	test_set={2:[],4:[]}
	train_set={2:[],4:[]}
	train_data=full_data[:-int(test_size*len(full_data))]
	test_data=full_data[-int(test_size*len(full_data)):]

	for i in train_data:
	    train_set[i[-1]].append(i[:-1])

	for i in test_data:
	    test_set[i[-1]].append(i[:-1])

	"""
	print train_set
	print  "#########"
	print test_set
	"""
	correct=0.0
	total=0.0

	for groups in test_set:
	    for features in test_set[groups]:
		vote = k_nearest_neighbors(train_set,features,k=5)
		#print type(vote)
		if vote == groups:
		     correct = correct +1
		total = total +1

	#print correct
	#print total 
        accuracy=correct/total   
	#print ("accuracy:",correct/total)
        accuracies.append(accuracy) 
	    
		   
	"""
	vote=k_nearest_neighbors(dataset,new_features,k=3)
	print (vote)
	"""
#print accuracies
print ("the total accuracy:",sum(accuracies)/len(accuracies))
