import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
import pandas as pd
from sklearn import preprocessing,cross_validation
style.use('ggplot')

x=np.array([[1,2],
            [2,2], 
            [2,3],
            [9,10],
            [10,10],
            [9,9]])
df = pd.read_excel("titanic3.xls")
df.drop(['name','boat','body'],1,inplace=True)
df.fillna(0,inplace=True)

def handle_non_numeric_data(df):
      columns = df.columns.values
      
      for column in columns:
          if df[column].dtype !=np.int64 and df[column].dtype !=np.float64 :
                text_vals={}
                def convert_to_int(val):
                    return text_vals[val]
                elements = df[column].values.tolist()
                unique_elements = set(elements)
                x = 0
                for i in unique_elements:
                    if i not in text_vals:
                         text_vals[i]=x
                         x = x+1
                df[column] = list(map(convert_to_int,df[column]))
      
      return df            
 
class K_means:
      def __init__(self,k=2,tol=0.001,max_iter=300):
            self.k = k
            self.tol = tol
            self.max_iter = max_iter
   
      def fit(self,data):
             self.centroids = {}
             self.data = data
             for i in range(self.k):
                  self.centroids[i] = data[i]
             
             
             for i in range(self.max_iter):
		     self.classifications = {0:[],
                                             1:[]}
                     for x in data:
		         distances =[np.linalg.norm(x - self.centroids[centroid])for centroid in self.centroids]
		         #print distances
		         classification = distances.index(min(distances))
	#	         print classification
		         self.classifications[classification].append(x)

		     #print self.centroids
		     #print self.classifications
		     previous_cent = dict(self.centroids)
		     original_cent = {}
		     for i in range(self.k):
		         self.centroids[i] = np.average(self.classifications[i],axis=0 )
		     
		     optimized = True
		     original_dict = self.centroids
		 #    print original_dict
		 #    print previous_cent
		     for i in range(self.k):
		         if(np.sum((original_dict[i]-previous_cent[i]/original_dict[i])*100)>self.tol):
	#	             print(np.sum((original_dict[i]-previous_cent[i]/original_dict[i])*100))
		             optimized = False
		     
		     if optimized :
		         break
             self.classifications={0:[],
                                   1:[]}
             for x in data:
                     distances = [np.linalg.norm(x - self.centroids[centroid])for centroid in self.centroids]
                     classification = distances.index(min(distances))
                     self.classifications[classification].append(x)
                     
                                 
             
      def predict(self,data):
               distances = [np.linalg.norm(data - self.centroids[centroid])for centroid in self.centroids] 
               classification = distances.index(min(distances))
               return classification           



df = handle_non_numeric_data(df)
#print df.head()

X = np.array(df.drop(['survived'],1)).astype(float)
X = preprocessing.scale(X)
Y = np.array(df['survived'])

X_train =X[:100]
X_test = X[-150:]

y_test = Y[-150:]

clf=K_means()
clf.fit(X_train)

correct = 0.0
for x in range(len(X_test)):
    prediction = clf.predict(X_test[x])
    if prediction == y_test[x]:
       correct +=1

print correct
print len(y_test)
print (correct/len(X_test)*100) 
    
"""
for centroid in centroids:
    plt.scatter(centroids[centroid][0], centroids[centroid][1],
                marker="o", color="k", s=150, linewidths=5)

colors = ["r", "g"] 

for classes in clf.classifications:
    for x in classifications[classes]:
        plt.scatter(x[0],x[1],s=100,marker="+",color = colors[classes],linewidths = 5) 

plt.show()
print centroids
"""
#print classifications
 
