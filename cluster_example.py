import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
from sklearn.cluster import KMeans


style.use('ggplot')

x=np.array([[1,2],[3,4],[2,2],[9,9],[10,10],[9,10]])

clf = KMeans(n_clusters=2)
clf.fit(x)

centroids = clf.cluster_centers_
labels = clf.labels_

colors=["r.","g.","b."]
print centroids 
print labels

for i in range(len(x)):
    plt.plot(x[i][0],x[i][1],colors[labels[i]],markersize=20)

for x in centroids:    
    plt.scatter(x[0],x[1],marker="+" ,s=20,linewidth=20,zorder=10)

plt.show()
         

