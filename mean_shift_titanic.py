import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.cluster import MeanShift


df = pd.read_excel("titanic3.xls")
original_df = pd.DataFrame.copy(df)
#prit df.head()
df.drop(['name','boat'],1,inplace = "True")
df.fillna(0,inplace = True)

def handle_non_numeric_data(df):
       columns = df.columns.values
  
       for column in columns:
           if df[column].dtype !=np.int64 and df[column].dtype !=np.float64:
                elements = df[column].values
                text_vals = {}
                def convert_to_int(val):
                     return text_vals[val]
                unique_elements = set(elements)
                x = 0
                for i in unique_elements:
                     if i not in text_vals:
                         text_vals[i] = x 
                 
                df[column]= list(map(convert_to_int,df[column]))
       
       return df
 

df = handle_non_numeric_data(df) 


X = np.array(df.drop(['survived'],1)).astype(float)

X = preprocessing.scale(X)            

Y = np.array(df['survived'].astype(float))

clf = MeanShift()
clf.fit(X)

cluster_centers = clf.cluster_centers_
labels = clf.labels_

original_df['cluster_group'] = np.nan

for i in range(len(labels)):
    original_df['cluster_group'].iloc[i] = labels[i]

values = np.array(original_df['cluster_group'].values).astype(float)
unique_groups =  np.unique(values)
survival_rate = {}
for i in unique_groups:
    i_group = original_df[original_df['cluster_group']==i]
    survival_cluster = i_group[i_group['survived']==1.0]
    survival_rate_num = float(len(survival_cluster))/len(i_group)
    survival_rate[i] = survival_rate_num
 
print survival_rate  
   
print (original_df[original_df['cluster_group']==6.0][:10])







