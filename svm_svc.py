import numpy as np
from sklearn import preprocessing,cross_validation,neighbors,svm
import pandas as pd

df=pd.read_csv("breast-cancer-wisconsin.data")
df.replace('?',-9999,inplace = True )
df.drop(['id'],1,inplace=True)

x=np.array(df.drop(['class'],1))
y=np.array(df['class'])


x_train,x_test,y_train,y_test=cross_validation.train_test_split(x,y,test_size=0.2)
clf=svm.SVC()

#train the data
clf.fit(x_train,y_train)

#test the classifier
accuracy=clf.score(x_test,y_test)
print (accuracy)

example_measure=np.array([[4,2,1,3,4,5,6,5,3],[4,5,1,1,1,1,2,1,3]])
#print (len(example_measure))
example_measure=example_measure.reshape(len(example_measure),-1)
prediction=clf.predict(example_measure)
print (prediction)


