import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
from sklearn.cluster import MeanShift
import pandas as pd



x= np.array([[1,2],
             [2,2],
             [1,1],
             [9,10],
             [10,10],
             [9,9],
             [5,5],
             [4,4],
             [4,5]])

clf = MeanShift()
clf.fit(x)
centroids = clf.cluster_centers_
print centroids
labels=clf.labels_
print labels
