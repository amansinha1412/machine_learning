import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn import preprocessing

df=pd.read_excel("titanic3.xls")
#print df.head()

df.drop(['name','body'],1,inplace=True)
df.convert_objects(convert_numeric=True)
df.fillna(0,inplace=True)

#print df['sex']

def handle_non_numeric_data(df):
    
    columns = df.columns.values
    
    for column in columns:
         text_to_vals={}
         def convert_to_int(text):
             return text_to_vals[text]

         if df[column].dtype != np.int64 and df[column].dtype != np.float64:
             elements = df[column].values.tolist()
             unique_elements = set(elements)
             x=0
             for element in unique_elements:
                 if element not in text_to_vals:
                     text_to_vals[element]=x
                     x+=1
             
             df[column] = list(map(convert_to_int, df[column]))

    return df
   
    

df = handle_non_numeric_data(df)

df.drop(['sex','boat'], 1, inplace=True)

X = np.array(df.drop(['survived'],1)).astype(float)
X=preprocessing.scale(X)
Y = np.array(df['survived'])

clf = KMeans(n_clusters = 2)
clf.fit(X)
labels = clf.labels_

print type(X[0]);

correct = 0.0
for i in range(len(X)):
    predict_me = np.array(X[i].astype(float))
    predict_me = predict_me.reshape(-1,len(predict_me))
    prediction = clf.predict(predict_me)
    
    if Y[i] == prediction :
       correct = correct +1

accuracy =correct/len(X) 
if accuracy > 0.5:
    print accuracy
else :
   print 1-accuracy

#print df.head()




